pub mod gen;

use apollo_core::{AllocationBlock, DataType};

/// Chip-8 Emulator
use arbitrary_int::{u12, u4};
// use inkwell::builder::Builder;

pub trait Chip8NibbleSelect {
    fn nibbles(&self) -> (u4, u4, u4, u4);
    fn value(&self) -> u16;
    /// Returns the second nibble (4 bits) of the instruction (the X nibble)
    #[inline(always)]
    fn x(&self) -> u4 {
        let (_, x, _, _) = Self::nibbles(self);
        x
    }
    /// Returns the third nibble (4 bits) of the instruction (the Y nibble)
    #[inline(always)]
    fn y(&self) -> u4 {
        let (_, _, y, _) = Self::nibbles(self);
        y
    }
    /// Returns the fourth nibble (4 bits) of the instruction (the N nibble)
    #[inline(always)]
    fn n(&self) -> u4 {
        let (_, _, _, n) = Self::nibbles(self);
        n
    }
    /// Returns the last byte or the last 2 nibbles (8 bits) of the instruction (the NN byte)
    #[inline(always)]
    fn nn(&self) -> u8 {
        (self.value() & 0x00FF) as u8
    }

    /// Returns the last 3 nibbles (12 bits) of the instruction (the NNN byte + nibble)
    #[inline(always)]
    fn nnn(&self) -> u12 {
        u12::new((self.value() & 0x0FFF) as u16)
    }
}

impl Chip8NibbleSelect for u16 {
    #[inline(always)]
    fn nibbles(&self) -> (u4, u4, u4, u4) {
        (
            u4::new(((self & 0xF000) >> 12) as u8),
            u4::new(((self & 0x0F00) >> 8) as u8),
            u4::new(((self & 0x00F0) >> 4) as u8),
            u4::new((self & 0x000F) as u8),
        )
    }

    #[inline(always)]
    fn value(&self) -> u16 {
        *self
    }
}

pub enum Chip8Instruction {
    /// 0NNN - Calls RCA 1802 program at address NNN. Not necessary for most ROMs.
    RCA1802 { address: u12 },
    /// 00E0 - Clears the screen.
    ClearScreen,
    /// 00EE - Returns from a subroutine.
    Return,
    /// 1NNN - Jumps to address NNN.
    Jump { address: u12 },
    /// 2NNN - Calls subroutine at NNN.
    Call { address: u12 },
    /// 3XNN - Skips the next instruction if register VX equals NN.
    SkipIfEqual { x: u4, nn: u8 },
    /// 4XNN - Skips the next instruction if register VX doesn't equal NN.
    SkipIfNotEqual { x: u4, nn: u8 },
    /// 5XY0 - Skips the next instruction if register VX equals VY.
    SkipIfEqualRegisters { x: u4, y: u4 },
    /// 6XNN - Sets VX to NN.
    SetRegister { x: u4, nn: u8 },
    /// 7XNN - Adds NN to the value of register VX.
    AddToRegister { x: u4, nn: u8 },
    /// 8XY0 - Sets the value of register VX to the value of register VY.
    SetRegisterToRegister { x: u4, y: u4 },
    /// 8XY1 - Sets the value of register VX to the bitwise OR of its current value and the value of register VY.
    SetRegisterToRegisterOr { x: u4, y: u4 },
    /// 8XY2 - Sets the value of register VX to the bitwise AND of its current value and the value of register VY.
    SetRegisterToRegisterAnd { x: u4, y: u4 },
    /// 8XY3 - Sets the value of register VX to the bitwise XOR of its current value and the value of register VY.
    SetRegisterToRegisterXor { x: u4, y: u4 },
    /// 8XY4 - Adds the value of register VY to the value of register VX. VF is set to 1 when there's a carry, and to 0 when there isn't.
    AddRegisterToRegister { x: u4, y: u4 },
    /// 8XY5 - VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
    SubtractRegisterFromRegister { x: u4, y: u4 },
    /// 8XY6 - Shifts VX right by one. VF is set to the value of the least significant bit of VX before the shift.
    ShiftRegisterRight { x: u4 },
    /// 8XY7 - Sets register VX to the value of VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
    SetRegisterToRegisterSubtract { x: u4, y: u4 },
    /// 8XYE - Shifts VX left by one. VF is set to the value of the most significant bit of VX before the shift.
    ShiftRegisterLeft { x: u4 },
    /// 9XY0 - Skips the next instruction if register VX doesn't equal VY.
    SkipIfNotEqualRegisters { x: u4, y: u4 },
    /// ANNN - Sets register I to the address NNN.
    SetI { address: u12 },
    /// BNNN - Jumps to the address NNN plus V0.
    JumpPlusV0 { address: u12 },
    /// CXNN - Sets VX to the result of a bitwise AND operation on a random number and NN.
    SetRegisterToRandom { x: u4, nn: u8 },
    /// DXYN - Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N pixels.
    DrawSprite { x: u4, y: u4, n: u4 },
    /// EX9E - Skips the next instruction if the key stored in VX is pressed.
    SkipIfKeyPressed { x: u4 },
    /// EXA1 - Skips the next instruction if the key stored in VX isn't pressed.
    SkipIfKeyNotPressed { x: u4 },
    /// FX07 - Sets VX to the value of the delay timer.
    SetRegisterToDelayTimer { x: u4 },
    /// FX0A - A key press is awaited, and then stored in VX.
    AwaitKeyPress { x: u4 },
    /// FX15 - Sets the delay timer to VX.
    SetDelayTimer { x: u4 },
    /// FX18 - Sets the sound timer to VX.
    SetSoundTimer { x: u4 },
    /// FX1E - Adds VX to I. If overflow, VF is set to 1, otherwise 0.
    AddRegisterToI { x: u4 },
    /// FX29 - Sets I to the location of the sprite for the character in VX.
    SetIToSprite { x: u4 },
    /// FX33 - Stores the binary-coded decimal representation of VX, with the most significant of three digits at the address in I, the middle digit at I plus 1, and the least significant digit at I plus 2.
    StoreBCD { x: u4 },
    /// FX55 - Stores V0 to VX (including VX) in memory starting at address I.
    StoreRegisters { x: u4 },
    /// FX65 - Fills V0 to VX (including VX) with values from memory starting at address I.
    LoadRegisters { x: u4 },
    // TODO: Implement Super Chip-48 instructions
}

// impl Chip8Instruction {
//     pub fn compile(&self, builder: &Builder) {
//         match self {
//             Self::AddToRegister { x, nn } => {}
//             _ => panic!("Not implemented"),
//         }
//     }
// }

pub fn parse_instruction(i: u16) -> Chip8Instruction {
    let (n1, n2, n3, n4) = i.nibbles();
    let (x, y, n, nn, nnn) = (i.x(), i.y(), i.n(), i.nn(), i.nnn());
    // The great match statement (ooh, ahh)
    match (n1.value(), n2.value(), n3.value(), n4.value()) {
        (0x0, 0x0, 0xE, 0x0) => Chip8Instruction::ClearScreen,
        (0x0, 0x0, 0xE, 0xE) => Chip8Instruction::Return,
        (0x0, _, _, _) => Chip8Instruction::RCA1802 { address: nnn },
        (0x1, _, _, _) => Chip8Instruction::Jump { address: nnn },
        (0x2, _, _, _) => Chip8Instruction::Call { address: nnn },
        (0x3, _, _, _) => Chip8Instruction::SkipIfEqual { x, nn },
        (0x4, _, _, _) => Chip8Instruction::SkipIfNotEqual { x, nn },
        (0x5, _, _, 0x0) => Chip8Instruction::SkipIfEqualRegisters { x, y },
        (0x6, _, _, _) => Chip8Instruction::SetRegister { x, nn },
        (0x7, _, _, _) => Chip8Instruction::AddToRegister { x, nn },
        (0x8, _, _, 0x0) => Chip8Instruction::SetRegisterToRegister { x, y },
        (0x8, _, _, 0x1) => Chip8Instruction::SetRegisterToRegisterOr { x, y },
        (0x8, _, _, 0x2) => Chip8Instruction::SetRegisterToRegisterAnd { x, y },
        (0x8, _, _, 0x3) => Chip8Instruction::SetRegisterToRegisterXor { x, y },
        (0x8, _, _, 0x4) => Chip8Instruction::AddRegisterToRegister { x, y },
        (0x8, _, _, 0x5) => Chip8Instruction::SubtractRegisterFromRegister { x, y },
        (0x8, _, _, 0x6) => Chip8Instruction::ShiftRegisterRight { x },
        (0x8, _, _, 0x7) => Chip8Instruction::SetRegisterToRegisterSubtract { x, y },
        (0x8, _, _, 0xE) => Chip8Instruction::ShiftRegisterLeft { x },
        (0x9, _, _, 0x0) => Chip8Instruction::SkipIfNotEqualRegisters { x, y },
        (0xA, _, _, _) => Chip8Instruction::SetI { address: nnn },
        (0xB, _, _, _) => Chip8Instruction::JumpPlusV0 { address: nnn },
        (0xC, _, _, _) => Chip8Instruction::SetRegisterToRandom { x, nn },
        (0xD, _, _, _) => Chip8Instruction::DrawSprite { x, y, n },
        (0xE, _, 0x9, 0xE) => Chip8Instruction::SkipIfKeyPressed { x },
        (0xE, _, 0xA, 0x1) => Chip8Instruction::SkipIfKeyNotPressed { x },
        (0xF, _, 0x0, 0x7) => Chip8Instruction::SetRegisterToDelayTimer { x },
        (0xF, _, 0x0, 0xA) => Chip8Instruction::AwaitKeyPress { x },
        (0xF, _, 0x1, 0x5) => Chip8Instruction::SetDelayTimer { x },
        (0xF, _, 0x1, 0x8) => Chip8Instruction::SetSoundTimer { x },
        (0xF, _, 0x1, 0xE) => Chip8Instruction::AddRegisterToI { x },
        (0xF, _, 0x2, 0x9) => Chip8Instruction::SetIToSprite { x },
        (0xF, _, 0x3, 0x3) => Chip8Instruction::StoreBCD { x },
        (0xF, _, 0x5, 0x5) => Chip8Instruction::StoreRegisters { x },
        (0xF, _, 0x6, 0x5) => Chip8Instruction::LoadRegisters { x },
        _ => panic!("Invalid instruction: {}", i),
    }
}

pub fn parse_rom(rom: Vec<u8>) -> Vec<Chip8Instruction> {
    let mut instructions: Vec<Chip8Instruction> = Vec::new();
    for i in rom.chunks_exact(2) {
        let instruction = u16::from_be_bytes([i[0], i[1]]);
        instructions.push(parse_instruction(instruction));
    }
    instructions
}

pub fn allocation_block() -> AllocationBlock {
    AllocationBlock {
        registers: (16, DataType::U8),
        memory: 4096,
        stack: (16, 2),
    }
}
