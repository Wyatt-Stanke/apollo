#[cfg(test)]
mod tests {
    use apollo_chip8::Chip8NibbleSelect;
    use arbitrary_int::{u12, u4};

    #[test]
    fn test_nibbles() {
        let instruction: u16 = 0xABCD;
        assert_eq!(
            instruction.nibbles(),
            (u4::new(0xA), u4::new(0xB), u4::new(0xC), u4::new(0xD))
        );
    }

    #[test]
    fn test_value() {
        let instruction: u16 = 0xABCD;
        assert_eq!(instruction.value(), 0xABCD);
    }

    #[test]
    fn test_x() {
        let instruction: u16 = 0xABCD;
        assert_eq!(instruction.x(), u4::new(0xB));
    }

    #[test]
    fn test_y() {
        let instruction: u16 = 0xABCD;
        assert_eq!(instruction.y(), u4::new(0xC));
    }

    #[test]
    fn test_n() {
        let instruction: u16 = 0xABCD;
        assert_eq!(instruction.n(), u4::new(0xD));
    }

    #[test]
    fn test_nn() {
        let instruction: u16 = 0xABCD;
        assert_eq!(instruction.nn(), 0xCD);
    }

    #[test]
    fn test_nnn() {
        let instruction: u16 = 0xABCD;
        assert_eq!(instruction.nnn(), u12::new(0xBCD));
    }
}
