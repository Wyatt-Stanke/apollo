use cranelift_codegen::ir::Block;
use cranelift_frontend::{FunctionBuilder, Variable};

pub struct VariableIndexManager {
    index: u32,
}

impl VariableIndexManager {
    pub fn new(offset: u32) -> Self {
        Self { index: offset }
    }

    pub fn var(&mut self) -> Variable {
        let var = Variable::from_u32(self.index);
        self.index += 1;
        var
    }
}

pub trait FunctionBuilderExt {
    fn switch_and_seal(&mut self, block: Block);
}

impl FunctionBuilderExt for FunctionBuilder<'_> {
    fn switch_and_seal(&mut self, block: Block) {
        self.seal_block(block);
        self.switch_to_block(block);
    }
}
