use crossterm::{style::*, ExecutableCommand};

enum Character {
    Normal(String),
    Inverted(String)
}

fn map_to_ascii(tl: bool, tr: bool, bl: bool, br: bool) -> Character {
    match (tl as u8, tr as u8, bl as u8, br as u8) {
        (0, 0, 0, 1) => Character::Normal("▗"),
        (0, 0, 1, 0) => Character::Normal("▖"),
        (0, 0, 1, 1) => Character::Normal("▄"),
        (0, 1, 0, 0) => Character::Normal("▝"),
        (0, 1, 0, 1) => Character::Normal("▐"),
        (0, 1, 1, 0) => Character::Normal("▞"),
        (1, 1, 1, 1) => Character::Normal("█"),
        (1, 0, 0, 0) => Character::Normal("▘"),
        (1, 0, 1, 0) => Character::Normal("▌"),
        (1, 0, 0, 1) => Character::Normal("▚"),
        // "▗"
        // "▖"
        // "▄"
    }
    .to_string()
}

fn main() {
    println!("Hello, world!");
}
