const audioDuration = 0.1; // 0.1 seconds
const pauseDuration = 1; // 1 second

function generateAudio() {
    const startTime = Date.now();
    const endTime = startTime + audioDuration * 1000;

    while (Date.now() < endTime) {
        process.stdout.write(String.fromCharCode(Math.random() * 100));
    }

    setTimeout(pause, pauseDuration * 1000);
}

function pause() {
    setTimeout(generateAudio, 0);
}

generateAudio();
