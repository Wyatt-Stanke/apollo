fn main() {
    let mut x = 3;
    let mut y = 4;
    add1(&mut x);
    add1(&mut y);
}

fn add1(x: &mut u32) {
    *x += 1;
}
