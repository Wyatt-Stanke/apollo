# Check for GNU sed and not BSD/OSX sed
if [[ $(sed --version 2>&1 | head -n 1) =~ "GNU" ]]; then
    SED=sed
else
    # Try checking for "gsed" instead
    if [[ $(gsed --version 2>&1 | head -n 1) =~ "GNU" ]]; then
        SED=gsed
    else
        echo "GNU sed not found"
        echo "Please install GNU sed or gsed"
        exit 1
    fi
fi

# Delete all files in out but not the out directory itself
rm -rf out/*
docker build -t foo . && docker run -it -v ./out:/out --rm foo

rm -f out/**/*.opt.clif out/**/*.vcode
# Remove everything that ISN'T *main*.unopt.clif
find out -type f ! -name '*main*.unopt.clif' -delete
# Move {x}.clif/*main*.unopt.clif to {x}.clif
find out -type f -name '*main*.unopt.clif' -exec sh -c 'mv "$1" "${1%/*}.clif"' _ {} \;
# Remove all directories in out
find out -type d -empty -delete
# Move *.clif.clif to *.clif
find out -type f -name '*.clif.clif' -exec sh -c 'mv "$1" "${1%.*}"' _ {} \;

# For each CLIF file,
for f in out/*.clif; do
    # Remove all lines that match the regex set [a-zA-Z_)0-9]+=[a-zA-Z_)0-9]*
    $SED -i '/[a-zA-Z_)0-9]\+=\([a-zA-Z_)0-9]\+\)/d' $f
    # Remove all blank lines that are at the start of the file
    $SED -i '/./,$!d' $f
done

shaHashes=()
for f in out/*.clif; do
    hash=$(sha256sum $f | cut -d' ' -f1)
    if [[ " ${shaHashes[@]} " =~ " ${hash} " ]]; then
        echo "Duplicate hash: $hash"
        rm $f
    else
        shaHashes+=($hash)
    fi
done

# If there's only one file in out, open it in code-insiders
if [[ $(ls out | wc -l) -eq 1 ]]; then
    code-insiders out/*.clif
# If there's only two files in out, diff them in code-insiders
elif [[ $(ls out | wc -l) -eq 2 ]]; then
    code-insiders -d out/*.clif
# If there's more than two files in out, open them in nvim
else
    nvim -d out/*.clif
fi